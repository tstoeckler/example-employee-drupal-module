# Event

This is an example Drupal module that provides _Event_ and _Event type_ entity
types which showcases the features of the _Entity API_, including:

* Computed fields
* Validation constraints
* Automatic UI generation
* Field UI integration
* Translation
* Revisions
* Bulk operations
* Views integration
