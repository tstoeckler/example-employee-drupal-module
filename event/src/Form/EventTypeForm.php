<?php

namespace Drupal\event\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

class EventTypeForm extends EntityForm {

  public function form(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->getEntity()->get('label'),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => [get_class($this->getEntity()), 'load'],
      ],
      '#default_value' => $this->getEntity()->get('label'),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->getEntity()->get('description'),
    ];

    $form['new_revision'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new revisions by default'),
      '#default_value' => $this->getEntity()->get('new_revision'),
    ];

    return parent::form($form, $form_state);
  }


}
