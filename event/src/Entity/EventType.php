<?php

namespace Drupal\event\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityDescriptionInterface;
use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * @ConfigEntityType(
 *   id = "event_type",
 *   label = @Translation("Event type"),
 *   label_collection = @Translation("Event types"),
 *   label_singular = @Translation("event type"),
 *   label_plural = @Translation("event types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event type",
 *     plural = "@count event types",
 *   ),
 *   admin_permission = "administer event types",
 *   bundle_of = "event",
 *   config_prefix = "type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "new_revision",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "add"    = "Drupal\event\Form\EventTypeForm",
 *       "edit"   = "Drupal\event\Form\EventTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\event\Controller\EventTypeListBuilder",
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "collection"  = "/admin/structure/event-types",
 *     "add-form"    = "/admin/structure/event-types/add",
 *     "edit-form"   = "/admin/structure/event-types/manage/{event_type}",
 *     "delete-form" = "/admin/structure/event-types/manage/{event_type}/delete",
 *   },
 * )
 */
class EventType extends ConfigEntityBundleBase implements EntityDescriptionInterface, RevisionableEntityBundleInterface {

  protected string $id;

  protected string $label;

  protected string $description;

  protected bool $new_revision = TRUE;

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

}
