<?php

namespace Drupal\event\Entity;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\event\Field\RemainingFieldItemList;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * @ContentEntityType(
 *   id = "event",
 *   label = @Translation("Event"),
 *   label_collection = @Translation("Events"),
 *   label_singular = @Translation("event"),
 *   label_plural = @Translation("events"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event",
 *     plural = "@count events",
 *   ),
 *   admin_permission = "administer events",
 *   collection_permission = "access event overview",
 *   translatable = TRUE,
 *   bundle_entity_type = "event_type",
 *   field_ui_base_route = "entity.event_type.edit_form",
 *   show_revision_ui = TRUE,
 *   base_table = "event",
 *   data_table = "event_data",
 *   revision_table = "event_revision",
 *   revision_data_table = "event_revision_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "owner" = "author",
 *     "published" = "published",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_author",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   constraints = {
 *     "EventAttendeeCount" = {},
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\UncacheableEntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\UncacheableEntityPermissionProvider",
 *     "form" = {
 *       "add"                     = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit"                    = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete"                  = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete"                  = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "revision-revert"         = "Drupal\Core\Entity\Form\RevisionRevertForm",
 *       "revision-delete"         = "Drupal\Core\Entity\Form\RevisionDeleteForm",
 *     },
 *     "list_builder" = "Drupal\event\Controller\EventListBuilder",
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *       "revision" = "Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   links = {
 *     "canonical"            = "/event/{event}",
 *     "collection"           = "/admin/content/events",
 *     "delete-multiple-form" = "/admin/content/events/delete",
 *     "add-page"             = "/admin/content/events/add",
 *     "add-form"             = "/admin/content/events/add/{event_type}",
 *     "edit-form"            = "/admin/content/events/manage/{event}",
 *     "delete-form"          = "/admin/content/events/manage/{event}/delete",
 *     "version-history"      = "/admin/content/events/manage/{event}/revisions",
 *     "revision"             = "/admin/content/events/manage/{event}/revisions/{event_revision}",
 *     "revision-revert-form" = "/admin/content/events/manage/{event}/revisions/{event_revision}/revert",
 *     "revision-delete-form" = "/admin/content/events/manage/{event}/revisions/{event_revision}/delete",
 *   },
 * )
 */
class Event extends EditorialContentEntityBase implements EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  use EntityOwnerTrait;

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', ['weight' => 0])
      ->setDisplayOptions('view', [
        'label' => 'visually_hidden',
        'weight' => 0,
      ]);

    $fields['date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'settings' => [
          'format_type' => 'html_date',
        ],
        'weight' => 0,
      ])
      ->setDisplayOptions('form', ['weight' => 10]);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', ['weight' => 20]);

    $fields['maximum'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Maximum number of attendees'))
      ->setSetting('min', 1)
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDefaultValue(10)
      ->setDisplayOptions('form', ['weight' => 30]);

    $fields['attendees'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Attendees'))
      ->setSetting('target_type', 'user')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRevisionable(TRUE)
      ->addConstraint('UniqueField')
      ->setDisplayOptions('view', ['weight' => 40])
      ->setDisplayOptions('form', ['weight' => 40]);

    $fields['remaining'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Remaining number of attendees'))
      ->setComputed(TRUE)
      ->setClass(RemainingFieldItemList::class)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 50,
      ]);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('Path'))
      ->setComputed(TRUE)
      ->setDisplayOptions('form', ['weight' => 0]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE);

    return $fields;
  }

  public function getDate(): DrupalDateTime {
    return $this->get('date')->date;
  }

  public function setDate(DrupalDateTime $date): static {
    return $this->set('date', $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
  }

  public function getDescription(): MarkupInterface {
    return $this->get('description')->processed;
  }

  public function setDescription(string $description, string $format): static {
    return $this->set('description', [
      'value' => $description,
      'format' => $format,
    ]);
  }

}
