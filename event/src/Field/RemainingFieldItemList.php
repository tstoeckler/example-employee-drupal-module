<?php

namespace Drupal\event\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\event\Entity\Event;

class RemainingFieldItemList extends FieldItemList {

  use ComputedItemListTrait;

  protected function computeValue() {
    $event = $this->getEntity();
    assert($event instanceof Event);
    $remaining = $event->get('maximum')->value - $event->get('attendees')->count();
    $this->list[0] = $this->createItem(0, $remaining);
  }

}
