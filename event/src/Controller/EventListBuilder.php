<?php

namespace Drupal\event\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity\BulkFormEntityListBuilder;
use Drupal\event\Entity\Event;

class EventListBuilder extends BulkFormEntityListBuilder {

  public function buildHeader() {
    return [
      'title' => $this->t('Title'),
      'date' => [
        'data' => $this->t('Date'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'attendees' => [
        'data' => $this->t('Attendee count'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'published' => [
        'data' => $this->t('Published'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
    ] + parent::buildHeader();
  }

  public function buildRow(EntityInterface $entity) {
    assert($entity instanceof Event);
    return [
      'title' => $entity->get('title')->view([
        'label' => 'visually_hidden',
      ]),
      'date' => $entity->get('date')->view([
        'label' => 'visually_hidden',
      ]),
      'attendees' => [
        '#markup' => $entity->get('attendees')->count(),
      ],
      'published' => $entity->get('published')->view([
        'label' => 'visually_hidden',
        'settings' => [
          'format' => 'unicode-yes-no',
        ],
      ]),
    ] + parent::buildRow($entity);
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->actions) {
      $arguments = ['@entities' => $this->entityType->getPluralLabel()];
      $actions = [
        'publish' => $this->t('Publish @entities', $arguments),
        'unpublish' => $this->t('Unpublish @entities', $arguments),
        'delete' => $this->t('Delete @entities', $arguments),
      ];
      foreach ($actions as $action_name => $action_label) {
        $action_id = 'event_' . $action_name . '_action';
        $this->actions[$action_id] = $this->actionStorage->create([
          'id' => $action_id,
          'label' => $action_label,
          'type' => $this->entityTypeId,
          'plugin' => "entity:{$action_name}_action:$this->entityTypeId"
        ]);
      }
    }

    return parent::buildForm($form, $form_state);
  }


}
