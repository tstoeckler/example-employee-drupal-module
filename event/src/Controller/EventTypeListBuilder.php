<?php

namespace Drupal\event\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\event\Entity\EventType;

class EventTypeListBuilder extends EntityListBuilder {

  public function buildHeader() {
    return [
      'label' => $this->t('Label'),
      'date' => [
        'data' => $this->t('New revision'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
    ] + parent::buildHeader();
  }

  public function buildRow(EntityInterface $entity) {
    assert($entity instanceof EventType);
    return [
      'title' => $entity->get('label'),
      'new_revision' => $entity->get('new_revision') ? '✔' : '✖',
    ] + parent::buildRow($entity);
  }

}
