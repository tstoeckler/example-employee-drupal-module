<?php

namespace Drupal\event\Plugin\Validation\Constraint;

use Drupal\event\Entity\Event;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class EventAttendeeCountConstraintValidator extends ConstraintValidator {

  public function validate(mixed $value, Constraint $constraint) {
    assert($constraint instanceof EventAttendeeCountConstraint);

    if (!($value instanceof Event)) {
      throw new UnexpectedTypeException($value, Event::class);
    }

    if ($value->get('remaining')->value < 0) {
      $this->context->addViolation($constraint->message, [
        '@maximum' => $value->get('maximum')->value,
      ]);
    }
  }

}
