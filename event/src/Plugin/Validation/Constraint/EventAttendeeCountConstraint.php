<?php

namespace Drupal\event\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

#[Constraint(
  id: 'EventAttendeeCount',
  label: new TranslatableMarkup('Event attendee count constraint'),
)]
class EventAttendeeCountConstraint extends SymfonyConstraint {

  public string $message = 'The event can only have @maximum attendees.';

}
