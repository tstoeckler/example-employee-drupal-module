<?php

namespace Drupal\department\Controller;

use Drupal\Core\Entity\DraggableListBuilderTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\department\Entity\Department;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DepartmentListBuilder extends EntityListBuilder implements FormInterface {

  use DraggableListBuilderTrait {
    buildHeader as traitBuildHeader;
    buildRow as traitBuildRow;
  }

  protected $limit = FALSE;

  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, FormBuilderInterface $form_builder) {
    parent::__construct($entity_type, $storage);

    $this->formBuilder = $form_builder;
    $this->weightKey = 'weight';
  }

  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('form_builder'),
    );
  }

  public function getFormId() {
    return "{$this->entityTypeId}_list";
  }

  public function buildHeader() {
    $header = [
      'label' => $this->t('Label'),
    ];
    $header += $this->traitBuildHeader();
    $header += parent::buildHeader();
    return $header;
  }

  public function buildRow(EntityInterface $entity) {
    assert($entity instanceof Department);
    $row = [
      'label' => $entity->get('label')->value,
    ];
    $row += $this->traitBuildRow($entity);
    $row += parent::buildRow($entity);
    return $row;
  }

  protected function getWeight(EntityInterface $entity): int|float {
    assert($entity instanceof Department);
    return $entity->get($this->weightKey)->value;
  }

  protected function setWeight(EntityInterface $entity, float|int $weight): EntityInterface {
    assert($entity instanceof Department);
    return $entity->set($this->weightKey, $weight);
  }

  protected function getEntityListQuery(): QueryInterface {
    $query = $this->getStorage()->getQuery()
      ->accessCheck()
      ->sort($this->weightKey)
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query;
  }

}
