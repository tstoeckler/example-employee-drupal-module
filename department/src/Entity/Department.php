<?php

namespace Drupal\department\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * @ContentEntityType(
 *   id = "department",
 *   label = @Translation("Department"),
 *   label_collection = @Translation("Departments"),
 *   label_singular = @Translation("department"),
 *   label_plural = @Translation("departments"),
 *   label_count = @PluralTranslation(
 *     singular = "@count department",
 *     plural = "@count departments",
 *   ),
 *   admin_permission = "administer departments",
 *   base_table = "department",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "add"    = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit"   = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\department\Controller\DepartmentListBuilder",
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   links = {
 *     "collection"  = "/admin/structure/departments",
 *     "add-form"    = "/admin/structure/departments/add",
 *     "edit-form"   = "/admin/structure/departments/manage/{department}/edit",
 *     "delete-form" = "/admin/structure/departments/manage/{department}/delete",
 *   },
 * )
 */
class Department extends ContentEntityBase {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Label'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', ['weight' => 0]);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Weight'))
      ->setRequired(TRUE)
      ->setSetting('min', -100)
      ->setSetting('max', 100)
      ->setDefaultValue(0);

    return $fields;
  }


}
