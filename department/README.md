# Department

This is an example Drupal module that provides an _Department_ entity type which
has a weight field which can be set by drag-and-drop reordering the entities on
the entity list.
