# Drupal Entity Examples

This repository provides various example Drupal modules showcasing features of
the _Entity API_. The main example module is the _Event_ module (in the `event`
directory). The additional example are:

* Using Drupal to access an external data structure in the _Employee_ module (in
  the `employee` directory)
* Providing a drag-and-drop entity list in the _Department_ module (in the
  `department` directory)
