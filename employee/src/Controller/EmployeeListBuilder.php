<?php

namespace Drupal\employee\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

class EmployeeListBuilder extends EntityListBuilder {

  public function buildHeader() {
    return [
      'first_name' => $this->t('First name'),
      'last_name' => $this->t('Last name'),
      'birth_date' => [
        'data' => $this->t('Birth date'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
    ] + parent::buildHeader();
  }

  public function buildRow(EntityInterface $entity) {
    return [
      'first_name' => $entity->get('first_name')->view(),
      'last_name' => $entity->get('last_name')->view(),
      'birth_date' => $entity->get('birth_date')->view(),
    ] + parent::buildRow($entity);
  }

}
