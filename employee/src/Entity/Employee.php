<?php

namespace Drupal\employee\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * @ContentEntityType(
 *   id = "employee",
 *   label = @Translation("Employee"),
 *   label_collection = @Translation("Employees"),
 *   label_singular = @Translation("employee"),
 *   label_plural = @Translation("employees"),
 *   label_count = @PluralTranslation(
 *     singular = "@count employee",
 *     plural = "@count employees",
 *   ),
 *   admin_permission = "administer employees",
 *   base_table = "employees",
 *   entity_keys = {
 *     "id" = "emp_no",
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "edit"   = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\employee\Controller\EmployeeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   links = {
 *     "collection" = "/admin/structure/employees",
 *     "canonical"  = "/admin/structure/employees/{employee}",
 *   },
 * )
 */
class Employee extends ContentEntityBase {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['birth_date'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Birth date'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 10)
      ->setDisplayOptions('form', ['weight' => 0]);

    $fields['first_name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('First name'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', ['weight' => 10]);

    $fields['last_name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Last name'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', ['weight' => 20]);

    $fields['gender'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Gender'))
      ->setSetting('allowed_values', [
        'F' => 'Female',
        'M' => 'Male',
        'N' => 'Non-binary',
        'O' => 'Other',
      ])
      ->setRequired(TRUE)
      ->setDisplayOptions('form', ['weight' => 30]);

    $fields['hire_date'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Hire date'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 10)
      ->setDisplayOptions('form', ['weight' => 40]);

    return $fields;
  }

}
