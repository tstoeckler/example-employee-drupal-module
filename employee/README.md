# Employee

This is an example Drupal module that provides an _Employee_ entity type which
allows exposing the data in the `employee` table of the example dataset at
[github.com/datacharmer/test_db](https://github.com/datacharmer/test_db) as
Drupal entities.

The module provides a list of employees at `/admin/content/employees` and an
edit and delete form for each employee, respectively.

## Disclaimer

Note that this is not an endorsement of the structure of the data set, in
particular storing of employees' gender or using simple first and last name
columns.

## Usage

Note that the example dataset is imported into an `employees` database that will
be created automatically. For Drupal to access the data, you need to install
this module first (which will create the `employees` table in the Drupal
database) and then copy the data from one database to the other by running

```sql
INSERT INTO mydrupaldatabase.employees SELECT * FROM employees.employees;
```
(replacing `mydrupaldatabase` with the actual name of your Drupal database).
